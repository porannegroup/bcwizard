#!/usr/bin/env python
"""
Call the main function of bcwizard
"""

from bcwizard.main import main

if __name__ == "__main__":
    main()
