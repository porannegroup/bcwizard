# This file is part of bcwizard.
#
# Copyright (C) 2021 ETH Zurich, Eno Paenurk, Renana Gershoni-Poranne
#
# bcwizard is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bcwizard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with bcwizard. If not, see <https://www.gnu.org/licenses/>.

"""
Classes for setting up the molecule
Adapted from the code used in https://doi.org/10.1063/5.0038292
Part of bcwizard, available at:
gitlab.com/porannegroup/bcwizard
"""

# standard imports
from dataclasses import dataclass

# non-standard imports
import networkx as nx
import numpy as np

# bcwizard imports
from bcwizard import const
from bcwizard import printers


class Molecule():
    """ Class for molecular representation and manipulation
    """

    def __init__(self, atoms: dict):
        """
        Args:
            atoms (dict): dictionary of mol.Atom objects
        """
        self.max_ring_size = 10
        self.atoms = atoms
        self._setup_molGraph()
        self._setup_cycles()
        self._setup_cycleGraph()
        
    def _setup_molGraph(self, tol=0.25):
        """Generate molecular graph

        Args:
            tol (float, optional): Tolerance w.r.t. covalent distance between atoms for bond recognition. Defaults to 0.25.
        """
        nodes = []
        self.molGraph = nx.Graph()

        # Make a node for each atom
        for atomID, atom in self.atoms.items():
            self.molGraph.add_node(atomID, element=atom.element)
            nodes.append(atomID)
        
        # Loop over all the unique atom pairs
        for i in range(len(nodes)):
            for j in range(i):
                # select the atoms
                atom_i = self.atoms[nodes[i]]
                atom_j = self.atoms[nodes[j]]
                # calculate the actual distance between the atoms
                d_act = np.linalg.norm(atom_i.get_coord() - atom_j.get_coord())
                # calculate the covalent radii based distance
                d_cov = const.__COV_RADII__[atom_i.element] + const.__COV_RADII__[atom_j.element]
                # Add bond if the actual distance is within the expected distance within tolerance
                if tol > d_act - d_cov:
                    edge = (nodes[i], nodes[j])
                    self.molGraph.add_edge(*edge)

    def _setup_cycles(self):
        """ Set up the cycles
        """
        self.cycles = {}
        cycleBasis = nx.minimum_cycle_basis(self.molGraph)
        counter = 0

        for idx, atomIDs in enumerate(cycleBasis):
            # Don't take rings with more atoms than defined by max_ring_size
            if len(atomIDs) > self.max_ring_size:
                counter +=1
                warn_str = 'Cycle found by networkx but ignored by bcwizard.'
                warn_str += '\nThe atom IDs of the cycle ignored: '  + str(atomIDs)
                warn_str += '\nIf this cycle should be included, max_ring_size in mol.py has to be changed.'
                warn_str += '\nCurrent max_ring_size: ' + str(self.max_ring_size)
                printers.bcw_warning(warn_str)
                continue
            cycleID = idx + 1 - counter
            atomIDs.sort()

            # Calculate center of the cycle
            center = np.zeros(3)
            for atomID in atomIDs:
                center += self.atoms[atomID].get_coord()
            center /= len(atomIDs)
            x,y,z = center[0], center[1], center[2]

            # Find bonds in the cycle
            bondIDs = []
            for bond in self.molGraph.edges:
                if bond[0] in atomIDs and bond[1] in atomIDs:
                    bondIDs.append(bond)

            # Find cross-product (to define direction of the first bond)
            # (the direction first bond is used to generate the cyclic graph)
            # (There might be issues with twisted/non-planar systems)
            vec1 = self.atoms[bondIDs[0][1]].get_coord() - self.atoms[bondIDs[0][0]].get_coord()
            vec2 = self.atoms[bondIDs[0][0]].get_coord() - center
            rot = np.sum(np.cross(vec2,vec1))
            # define the default cycle to rotate clockwise
            if rot > 0:
                bondIDs[0] = bondIDs[0][::-1]
            
            # Generate Cycle object with weight 0
            self.cycles[cycleID] = Cycle(cycleID, atomIDs, bondIDs, x,y,z, weight=0)

    def _setup_cycleGraph(self):
        """ Set up graph of cycles
        """
        self.cycleGraph = nx.Graph()
        cycleIDs = []
        for cID, cycle in self.cycles.items():
            self.cycleGraph.add_node(cID, cycle=cycle)
            cycleIDs.append(cID)

        for i in range(len(self.cycles)):
            cycleID1 = cycleIDs[i]
            atomIDs_i = self.cycles[cycleID1].atomIDs
            for j in range(i):
                cycleID2 = cycleIDs[j]
                atomIDs_j = self.cycles[cycleID2].atomIDs
                for atomID in atomIDs_i:
                    if atomID in atomIDs_j:
                        self.cycleGraph.add_edge(cycleID1, cycleID2)
                        break

    def set_cycle_weights(self, weights: list):
        """ Update weights for the cycles

        Args:
            weights (list): New weights
        """
        for idx, w in enumerate(weights):
            self.cycles[idx+1].set_bcGraph(w)

    def set_bcGraph(self):
        """ Generate bond currents graph based on cycles and their weights
        """
        new_bcGraph = nx.DiGraph()

        # loop over all the cycles
        for cycle in self.cycles:
            # loop over all the bonds in the cycle
            for bond in self.cycles[cycle].bcGraph.edges:
                # if the bond is not yet part of the graph, create it
                if not bond in new_bcGraph.edges and not bond[::-1] in new_bcGraph.edges:
                    new_bcGraph.add_edge(*bond, weight=self.cycles[cycle].weight)
                # else add the contribution of the bond in the cycle to the bond in the whole molecule
                # choose the sign based on whether the bond is reported in the same atom order in the two graphs
                else:
                    if bond in new_bcGraph.edges:
                        new_bcGraph.edges[bond]['weight'] += self.cycles[cycle].weight
                    else:
                        bond = bond[::-1]
                        new_bcGraph.edges[bond]['weight'] -= self.cycles[cycle].weight
                        
        self.bcGraph = new_bcGraph


    def get_bcGraph(self):
        return self.bcGraph.copy()


    def get_molGraph(self, skip_hydrogen=True):
        """Get the molecular graph

        Args:
            skip_hydrogen (bool, optional): Do not include H atoms. Defaults to True.

        Returns:
            nx.graph: molecular graph
        """
        if skip_hydrogen:
            sub_nodes = []
            for node in self.molGraph.nodes:
                if self.molGraph.nodes[node]['element'] != 'H':
                    sub_nodes.append(node)
            newGraph = nx.subgraph(self.molGraph, sub_nodes)
            return newGraph
        else:
            return self.molGraph.copy()


@dataclass
class Cycle:
    """
    Is basically a subclass of knot.Knot

    Attributes
    ----------
    cycleID
    atomIDs:
        monocyclic atomIDs

    x,y,z
    """
    cycleID: int
    atomIDs: list
    bondIDs: list
    x: float
    y: float
    z: float
    weight: float

    def __post_init__(self):
        self.set_bcGraph(self.weight)

    def get_center(self):
        return np.array([self.x, self.y, self.z])

    def set_center(self, coordinate):
        self.x = coordinate[0]
        self.y = coordinate[1]
        self.z = coordinate[2]

    def set_bcGraph(self, weight):
        self.weight = weight
        self.bcGraph = nx.DiGraph()
        # Make a circular graph
        temp_bonds = self.bondIDs.copy() # make copy of bonds
        # append the first bond
        self.bcGraph.add_edge(*temp_bonds[0], weight=self.weight) 
        # set the second atom as reference
        ref_atom = temp_bonds[0][1] 
        # remove the used bond from the temp list
        temp_bonds.remove(temp_bonds[0]) 
        # redo until all bonds are assigned
        while len(temp_bonds) > 0:       
            for bond in temp_bonds:
                if bond[0] == ref_atom:
                    self.bcGraph.add_edge(*bond, weight=self.weight)
                    ref_atom = bond[1]
                elif bond[1] == ref_atom:
                    self.bcGraph.add_edge(*bond[::-1], weight=self.weight)
                    ref_atom = bond[0]
                else:
                    continue
                temp_bonds.remove(bond)


@dataclass
class Atom:
    """
    Corresponds to mol.Atom

    Data:
    ID (atomID)
    element symbol
    x,y,z
    """
    atomID: int
    element: str
    x: float
    y: float
    z: float

    def get_coord(self):
        return np.array([self.x, self.y, self.z])

    def set_coord(self, coords):
        self.x = coords[0]
        self.y = coords[1]
        self.z = coords[2]
