# This file is part of bcwizard.
#
# Copyright (C) 2021 ETH Zurich, Eno Paenurk, Renana Gershoni-Poranne
#
# bcwizard is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bcwizard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with bcwizard. If not, see <https://www.gnu.org/licenses/>.

"""
NICS2BC calculators
Part of bcwizard, available at:
gitlab.com/porannegroup/bcwizard
"""

# non-standard imports
import numpy as np
from scipy.optimize import minimize


def bc_simplex_opt(molecule, calculator, ref_nics, ref_coords, component):
    """Simplex algorithm for optimizing the cycle weights via Biot-Savart
    calcutions with bond currents. Handles any number of reference points.

    Args:
        molecule (Molecule object): molecule's representation
        calculator (BiotSavart object): B-S calculator object
        ref_nics (np.array): reference NICS values
        ref_coords (np.array): reference coordinates for the NICS values

    Returns:
        float, array: rmsd, optimized weights
    """

    # Optimization function
    def opt_fun(weights):
        # update cycle weights in molecule
        molecule.set_cycle_weights(weights)
        # update bond currents
        molecule.set_bcGraph()
        # generate new points for integration
        calculator.gen_points(molecule.bcGraph)
        # calculate nics
        nics = calculator.calc_nics(ref_coords, component=component)
        # calculate rmsd
        rmsd = np.sum((nics - ref_nics)**2) / len(nics)
        print('RMSD(NICS): %7.5f' % rmsd)
        return rmsd

    print('Simplex optimization:')
    # Initialize weights randomly around 1
    init = [1.0+np.random.rand()/10 for i in range(len(molecule.cycles))]
    # optimize weights
    opt = minimize(opt_fun, np.asarray(init), method = 'Nelder-Mead', tol=1e-3)

    return opt.fun, opt.x


def bc_matrix_calc(molecule, calculator, ref_nics, ref_coords, component):
    """Calculate cycle weights based on the matrix equation via Biot-Savart
    calcutions with bond currents.

    Args:
        molecule (Molecule object): molecule's representation
        calculator (BiotSavart object): B-S calculator object
        ref_nics (np.array): reference NICS values
        ref_coords (np.array): reference coordinates for the NICS values

    Returns:
        array: calculated weights
    """

    # convert ref_nics to np.array in case it is not
    ref_nics = np.asarray(ref_nics)
    # count rings and nics
    n_rings = len(molecule.cycles)
    n_nics = len(ref_nics)
    # initialize the weights to unity in case they are not
    molecule.set_cycle_weights(np.ones(n_rings))
    
    # Calculate the sigma^tilde matrix (A)
    A = np.zeros((n_nics,n_rings))
    for i in range(n_rings):
        # generate new points for integration based on the cycle
        calculator.gen_points(molecule.cycles[i+1].bcGraph)
        # Populate the i-th column with the nics values
        A[:,i] = calculator.calc_nics(ref_coords, component=component)

    # Calculate the weights
    if n_rings == n_nics:
        w = np.dot(np.linalg.inv(A), ref_nics)
    # Use OLS if number of rings is different than number of NICS values
    else:
        w = np.dot(np.dot(np.linalg.inv(np.dot(A.T,A)),A.T), ref_nics)

    return w


def huckel_matrix_calc(molecule, ref_nics, alpha = -30.0, beta = 3.0):
    """Calculate cycle weights based on the matrix equation via Hückel-like
    approximation. Number of cycles should equal number of NICS.

    Args:
        molecule (Molecule object): molecule's representation
        ref_nics (np.array): reference NICS values
        alpha (float): diagonal element value
        beta (float): neighboring ring effect value

    Returns:
        array: calculated weights
    """

    # convert ref_nics to np.array in case it is not
    ref_nics = np.asarray(ref_nics)
    # count rings and nics
    n_rings = len(molecule.cycles)
    n_nics = len(ref_nics)
    if not n_rings == n_nics:
        print('The number of NICS points should equal the number of rings for BC matrix calculation.')
        print('Aborting.')
        return
    
    # Calculate the sigma^tilde matrix (A)
    # Populate the diagonal with alpha
    A = np.identity(n_rings) * alpha
    # Loop over adjacent cycles to populate with beta
    for cycle_connection in molecule.cycleGraph.edges:
        i = cycle_connection[0] - 1
        j = cycle_connection[1] - 1
        A[i,j] = beta
        A[j,i] = beta

    # Calculate the weights
    w = np.dot(np.linalg.inv(A), ref_nics)
    return w