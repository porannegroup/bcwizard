# This file is part of bcwizard.
#
# Copyright (C) 2021 ETH Zurich, Eno Paenurk, Renana Gershoni-Poranne
#
# bcwizard is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bcwizard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with bcwizard. If not, see <https://www.gnu.org/licenses/>.

"""
Biot-Savart calculations
Adapted from the code used in https://doi.org/10.1063/5.0038292
Part of bcwizard, available at:
gitlab.com/porannegroup/bcwizard
"""

# non-standard imports
import numpy as np

# bcwizard imports
from bcwizard import const


class BiotSavart():
    """ Biot-Savart calculator
    """

    """
    Prefactor containing the constants (mu0/(4*pi)).
    Atomic units: 5.3251e-5
    SI: 1.0000e-7
    """
    prefactor = {'au': 5.3251e-5, 'si': 1.0e-7}
    # coordinate indices for vectors
    coord_idx = {'x':0, 'y':1, 'z':2}

    def __init__(self, molecule, I_ref, n_points = 100, unit = 'au'):
        """
        Args:
            molecule: molecule object as defined in mol.py
            I_ref (float): reference current strength (in nA/T)
            n_points (int, optional): Number of points per bond used in the integration. Defaults to 100.
            unit (str, optional): Unit system used in the calculation (si or au). Defaults to 'au'.
        """
        # set up the variables
        self.mol = molecule
        self.I_ref = I_ref
        self.n_points = n_points
        self.unit = unit
        if self.unit == 'au':
            self.I_ref *= const.bc_si2au
        # generate the points for integration
        self.gen_points(self.mol.bcGraph)

    def gen_points(self, bcGraph):
        """Generates the points for integration

        Args:
            bcGraph: bond-current graph object
        """
        # list for differential elements
        dls = []
        # list for coordinates
        bond_points = []
        # list for current strength values
        Is = []

        # add data to the lists for each bond in the bond-current graph
        for bond in bcGraph.edges:
            # define the bond vector
            r_a = self.mol.atoms[bond[0]].get_coord()
            r_b = self.mol.atoms[bond[1]].get_coord()
            v_bond = r_b - r_a
            # generate points along the bond
            points = np.array([list(r_a+i*v_bond) for i in np.linspace(1/self.n_points, 1, self.n_points)][:-1])
            # generate differential elements
            dl = v_bond/np.linalg.norm(v_bond)/self.n_points
            # append data to the lists
            dls.append(np.array(self.n_points*[dl])[1:])
            bond_points.append(points)
            Is.append(bcGraph.edges[bond]['weight'] * self.I_ref * np.ones(self.n_points-1))

        # define the variables based on the lists
        self.rs = np.array([item for sublist in bond_points for item in sublist])
        self.dls = np.array([item for sublist in dls for item in sublist])
        self.Is = np.array([item for sublist in Is for item in sublist])

        if self.unit == 'au':
            self.rs *= const.ang2bohr
            self.dls *= const.ang2bohr


    def calc_Bind(self, probe_coords):
        """Calculate the induced magnetic field

        Args:
            probe_coords (list): coordinates at which to calculate B_ind

        Returns:
            array: induced magnetic field vectors
        """
        # initialize the containers
        dummies = probe_coords.copy()
        B_ind = np.zeros((len(dummies),3))
        # total length
        N = len(self.Is)
        # modify dummies if necessary
        if len(dummies.shape) < 2:
            dummies = dummies.reshape((1,3))
        if self.unit == 'au':
            dummies *= const.ang2bohr

        # calculate the product of differential elements and current strengths
        fac1 = self.dls * self.Is.reshape(N,1)
        # loop over the coordinates
        for idx, dummy in enumerate(dummies):
            # calculate the vectors from the currents to the dummy point
            vectors = dummy - self.rs
            # calculate the vectors divided by their norm cubed
            fac2 = vectors/(np.linalg.norm(vectors, axis=1)**3).reshape(N,1) 
            # calculate the cross products and sum them up
            B_ind[idx] = np.sum(np.cross(fac1, fac2), axis=0)
            
        return self.prefactor[self.unit] * B_ind


    def calc_nics(self, probe_coords, component = 'z'):
        """Calculate NICS

        Args:
            dummies (list/array): coordinates at which to calculate NICS
            component (str, optional): NICS component. Defaults to 'z'.

        Returns:
            array: NICS values
        """
        # nics is not defined as a negative here, because
        # nics = -sigma = B_ind / B_ext
        # and here we assume B_ext to be unity
        nics = self.calc_Bind(probe_coords) * 1e6
        return nics.T[self.coord_idx[component]]
