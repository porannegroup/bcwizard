# This file is part of bcwizard.
#
# Copyright (C) 2021 ETH Zurich, Eno Paenurk, Renana Gershoni-Poranne
#
# bcwizard is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bcwizard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with bcwizard. If not, see <https://www.gnu.org/licenses/>.

# bcwizard imports
import bcwizard.calc.nics2bc
import bcwizard.calc.biotsavart
import bcwizard.calc.height

# reference XY coordinates for benzene carbons
_bnz_C_xy_coords = [
[-1.204878, 0.695656],
[0.000015, 1.391256],
[1.204878, 0.695609],
[1.204878, -0.695656],
[-0.000015, -1.391256],
[-1.204878, -0.695609]
]