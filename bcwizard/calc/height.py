# This file is part of bcwizard.
#
# Copyright (C) 2021 ETH Zurich, Eno Paenurk, Renana Gershoni-Poranne
#
# bcwizard is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bcwizard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with bcwizard. If not, see <https://www.gnu.org/licenses/>.

"""
Height optimizer
Part of bcwizard, available at:
gitlab.com/porannegroup/bcwizard
"""

# non-standard imports
import numpy as np
from scipy.optimize import minimize_scalar

# bcwizard imports
from bcwizard import dummyman

def opt_height(molecule, calculator, ref_nics, component):
    """Optimizes the height to reproduce the reference NICS with the specified calculator

    Args:
        molecule: molecule object as defined in mol.py
        calculator: calculator object (Biot-Savart)
        ref_nics (list/array): reference NICS values
        component (str): component of NICS
    """

    # define the RMSD calculator used for minimization
    def opt_fun(height, molecule, calculator, ref_nics):
        # generate the probe at the input height
        probe = dummyman.gen_dummies(molecule, height)
        # calculate the NICS value
        nics = calculator.calc_nics(probe, component=component)
        # calculate RMSD to the reference NICS
        rmsd = np.sum((nics - ref_nics)**2)
        return rmsd

    # Optimize the height
    opt = minimize_scalar(opt_fun, bounds=(0.01, 5.0), method='bounded', 
                          args=(molecule, calculator, ref_nics))
    return opt.x