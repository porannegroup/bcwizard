"""
Main function of bcwizard
Calls interactive menu
"""

from bcwizard import interactivemenu

def main():
    interactivemenu.InteractiveMenu()


