"""
Functions to manage dummy atoms (nics probes)
Part of bcwizard, available at:
gitlab.com/porannegroup/bcwizard
"""

import numpy as np

def gen_dummies(molecule, height, gentype = 'positive'):
    """Generate dummy coordinates for each cycle in molecule at the specified height

    Args:
        molecule: molecule object as defined in mol.py
        height (float): height of the dummies
        gentype (str, optional): Type of dummy generation; options: positive or rmsd_based. Defaults to 'positive'.

    Returns:
        np.array: coordinates of dummy atoms
    """

    dummies = []

    # rmsd calculator
    def _calc_rmsd(atom, list_of_coords):
        rmsd = 0
        for i in list_of_coords:
            rmsd += np.linalg.norm(atom - i)
        return rmsd

    # make a list of the atomic coordinates for rmsd calculations
    atom_coords = []
    for a in molecule.atoms:
        atom_coords.append(molecule.atoms[a].get_coord())

    # Loop over all cycles
    for i in molecule.cycles:
            # get the center coordinates
            center = molecule.cycles[i].get_center()

            # Calculates the normal vector (according to the center and the first bond)
            # (could be made more robust)
            bond_atoms = molecule.cycles[i].bondIDs[0]
            bond_coords = []
            for a in bond_atoms:
                bond_coords.append(molecule.atoms[a].get_coord())
            normal = np.cross(center - bond_coords[0], center - bond_coords[1])
            normal /= np.linalg.norm(normal)

            # calculate two dummy options
            dummy1 = center + height * normal
            dummy2 = center - height * normal

            # if only positive z axis dummies
            if gentype == 'positive':
                if dummy1[2] > 0:
                    dummies.append(dummy1)
                else:
                    dummies.append(dummy2)

            # if rmsd based dummy generation
            elif gentype == 'rmsd_based':
                # calculate rmsd w.r.t. other dummies
                rmsd1_dummies = _calc_rmsd(dummy1, dummies)
                rmsd2_dummies = _calc_rmsd(dummy2, dummies)
                # calculate rmsd w.r.t. all atoms
                rmsd1_atoms = _calc_rmsd(dummy1, atom_coords)
                rmsd2_atoms = _calc_rmsd(dummy2, atom_coords)
                # if rmsd w.r.t. atoms is equal, check w.r.t. other dummies
                if round(rmsd1_atoms,1) == round(rmsd2_atoms,1):
                    if rmsd2_dummies < rmsd1_dummies:
                        dummies.append(dummy2)
                    else:
                        dummies.append(dummy1)
                # otherwise put dummy as far away from other atoms as possible
                elif rmsd1_atoms > rmsd2_atoms:
                    dummies.append(dummy1)
                else:
                    dummies.append(dummy2)

    return np.asarray(dummies)


def assign_centers(molecule, ref_coords):
    """Assigns the reference coordinates to rings based on distances

    Args:
        molecule: molecule object as defined in mol.py
        ref_coords (list/array): coordinates to assign to rings

    Returns:
        list: indices that can be used to rearrange the coordinates and related lists (e.g. NICS values)
    """
    # initialize the index list
    idxs = []
    for i in molecule.cycles:
        # get the coordinates for the center of the cycle
        center = molecule.cycles[i].get_center()
        # calculate the distances to ref_coords
        distances = np.linalg.norm(ref_coords - center, axis=1)
        # get the index for the closest distance and add it to idxs
        idx = np.argmin(distances)
        idxs.append(idx)
    return idxs


def adjust_ref_height(molecule, ref_coords, new_height):
    """Regenerate dummy coordinates for each cycle in molecule at the specified height

    Args:
        molecule: molecule object as defined in mol.py
        ref_coords (list/array): initial coordinates which to regenerate
        new_height (float): height of the dummies

    Returns:
        np.array: new coordinates of dummy atoms
    """

    # initialize new coords
    new_coords = np.zeros_like(ref_coords)
    # regenerate the probes at the new height
    for i in molecule.cycles:
        # get the coordinates for the center of the cycle
        center = molecule.cycles[i].get_center()
        # find the closest matching reference coordinate and get its index
        # this ensures that the new coords will have the same ordering as ref_coords
        distances = np.linalg.norm(ref_coords - center, axis=1)
        idx = np.argmin(distances)
        ref_coord = ref_coords[idx]

        # Calculates the normal vector (according to the center and the first bond)
        bond_atoms = molecule.cycles[i].bondIDs[0]
        bond_coords = []
        for a in bond_atoms:
            bond_coords.append(molecule.atoms[a].get_coord())
        normal = np.cross(center - bond_coords[0], center - bond_coords[1])
        normal /= np.linalg.norm(normal)

        # calculate two coordinate options
        new_coord1 = center + new_height * normal
        new_coord2 = center - new_height * normal

        # choose the option closer to the original coordinates
        if np.linalg.norm(new_coord1 - ref_coord) < np.linalg.norm(new_coord2 - ref_coord):
            new_coords[idx] = new_coord1
        else:
            new_coords[idx] = new_coord2

    return new_coords 
