# This file is part of bcwizard.
#
# Copyright (C) 2021 ETH Zurich, Eno Paenurk, Renana Gershoni-Poranne
#
# bcwizard is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bcwizard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with bcwizard. If not, see <https://www.gnu.org/licenses/>.

"""
Constants
Adapted from the code used in https://doi.org/10.1063/5.0038292
Part of bcwizard, available at:
gitlab.com/porannegroup/bcwizard
"""


# conversion between au and SI units
# based on constants from https://physics.nist.gov/cuu/Constants/index.html (accessed Apr 2021)
bc_si2au = 1 / (6.6236182375e-3 / 2.350517567e5) # current strength A/T
ang2bohr = 1 / 5.291772109e-1

# Element strings. Accessing index corresponds to the elemntal
# nr of the corresponding element.
__ATOM_LIST__ = \
    ['', 'H', 'He',
     'Li', 'Be', 'B',  'C',  'N',  'O',  'F',  'Ne',
     'Na', 'Mg', 'Al', 'Si', 'P',  'S',  'Cl', 'Ar',
     'K',  'Ca', 'Sc', 'Ti', 'V ', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu',
     'Zn', 'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr',
     'Rb', 'Sr', 'Y',  'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 'Ag',
     'Cd', 'In', 'Sn', 'Sb', 'Te', 'I',  'Xe',
     'Cs', 'Ba', 'La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy',
     'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf', 'Ta', 'W',  'Re', 'Os', 'Ir', 'Pt',
     'Au', 'Hg', 'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn',
     'Fr', 'Ra', 'Ac', 'Th', 'Pa', 'U',  'Np', 'Pu']

# Covalent radii of the elements in the periodic table in Angstrom
# Alvarez et. al., Covalent radii revisited, Dalton Trans.,2008, 2832-2838
# DOI: 10.1039/b801115j
__COV_RADII__ = \
    {
        "H" : 0.31,
        "He" : 0.28,
        "Li" : 1.28,
        "Be" : 0.96,
        "B" : 0.84,
        "C" : 0.76,
        "N" : 0.71,
        "O" : 0.66,
        "F" : 0.57,
        "Ne" : 0.58,
        "Na" : 1.66,
        "Mg" : 1.41,
        "Al" : 1.21,
        "Si" : 1.11,
        "P" : 1.07,
        "S" : 1.05,
        "Cl" : 1.02,
        "Ar" : 1.06,
        "K" : 2.03,
        "Ca" : 1.76,
        "Sc" : 1.7,
        "Ti" : 1.6,
        "V" : 1.53,
        "Cr" : 1.39,
        "Mn" : 1.39,
        "Fe" : 1.32,
        "Co" : 1.26,
        "Ni" : 1.24,
        "Cu" : 1.32,
        "Zn" : 1.22,
        "Ga" : 1.22,
        "Ge" : 1.2,
        "As" : 1.19,
        "Se" : 1.2,
        "Br" : 1.2,
        "Kr" : 1.16,
        "Rb" : 2.2,
        "Sr" : 1.95,
        "Y" : 1.9,
        "Zr" : 1.75,
        "Nb" : 1.64,
        "Mo" : 1.54,
        "Tc" : 1.47,
        "Ru" : 1.46,
        "Rh" : 1.42,
        "Pd" : 1.39,
        "Ag" : 1.45,
        "Cd" : 1.44,
        "In" : 1.42,
        "Sn" : 1.39,
        "Sb" : 1.39,
        "Te" : 1.38,
        "I" : 1.39,
        "Xe" : 1.4,
        "Cs" : 2.44,
        "Ba" : 2.15,
        "La" : 2.07,
        "Ce" : 2.04,
        "Pr" : 2.03,
        "Nd" : 2.01,
        "Pm" : 1.99,
        "Sm" : 1.98,
        "Eu" : 1.98,
        "Gd" : 1.96,
        "Tb" : 1.94,
        "Dy" : 1.92,
        "Ho" : 1.92,
        "Er" : 1.89,
        "Tm" : 1.9,
        "Yb" : 1.87,
        "Lu" : 1.87,
        "Hf" : 1.75,
        "Ta" : 1.7,
        "W" : 1.62,
        "Re" : 1.51,
        "Os" : 1.44,
        "Ir" : 1.41,
        "Pt" : 1.36,
        "Au" : 1.36,
        "Hg" : 1.32,
        "Tl" : 1.45,
        "Pb" : 1.46,
        "Bi" : 1.48,
        "Po" : 1.4,
        "At" : 1.5,
        "Rn" : 1.5,
        "Fr" : 2.6,
        "Ra" : 2.21,
        "Ac" : 2.15,
        "Th" : 2.06,
        "Pa" : 2.0,
        "U" : 1.96,
        "Np" : 1.9,
        "Pu" : 1.87,
        "Am" : 1.8,
        "Cm" : 1.69
    }
