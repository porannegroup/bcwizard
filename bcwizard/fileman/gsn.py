# This file is part of bcwizard.
#
# Copyright (C) 2021 ETH Zurich, Eno Paenurk, Renana Gershoni-Poranne
#
# bcwizard is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bcwizard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with bcwizard. If not, see <https://www.gnu.org/licenses/>.

"""
Read & write Gaussian files
Part of bcwizard, available at:
gitlab.com/porannegroup/bcwizard
"""

# non-standard imports
import numpy as np

# bcwizard imports
from bcwizard import const, mol
from bcwizard import fileman


def read_gsn_log(gsn_logfile, ncs=False, mos=None):
    """Read Gaussian log file

    Args:
        gsn_logfile (str): Path to the Gaussian log file
        ncs (bool, optional): Read NCS results from the log. Defaults to False.
        mos (str, optional): Comma-separated list of MO numbers (if ncs=True). Defaults to None.

    Returns:
        atoms, ref_coords, ref_tensors
    """

    # initalize the result containers
    atoms = {}
    ref_coords = []
    ref_tensors = []
    # define the indexes for getting the sigma values
    sigma_idxs = [1,3,5]

    # read the log file
    with open(gsn_logfile, 'r') as f:
        content = f.readlines()

    # initialize conditions used for reading the log
    read_xyz = False
    counter = False
    count = 5

    read_sigma = False
    sigma_count = 3
    if ncs:
        dummy_tensor = np.zeros((3,3))
    else:
        dummy_tensor = []

    check_mo = False

    # loop over all the lines
    for line in content:

        # for getting xyz
        if counter:
            count -= 1
            if count == 0:
                read_xyz = True
                counter = False

        if 'Input orientation' in line:
            counter = True     
        if '-'*20 in line:
            read_xyz = False

        if read_xyz:
            line = line.split()
            element = const.__ATOM_LIST__[int(line[1])]
            atomID = int(line[0])
            x,y,z = float(line[3]), float(line[4]), float(line[5])
            if element:
                atoms[atomID] = mol.Atom(atomID, element, x, y, z)
            else:
                ref_coords.append([x, y, z])

        # for getting sigma values
        if not ncs:
            if 'Bq   Isotropic' in line:
                read_sigma = True
                continue

            if read_sigma:
                sigma_count -= 1
                line = line.split()
                dummy_tensor.append([line[i] for i in sigma_idxs])

                if sigma_count == 0:
                    ref_tensors.append(dummy_tensor)
                    sigma_count = 3
                    read_sigma = False
                    dummy_tensor = []

        else:
            if check_mo and 'Canonical MO contributions' in line:
                read_sigma = True
                check_mo = False
                continue

            if 'Full Cartesian NMR shielding tensor (ppm) for atom gh' in line:
                check_mo = True

            if read_sigma:
                line = line.split()
                if 'Total' in line:
                    ref_tensors.append(dummy_tensor)
                    read_sigma = False
                    dummy_tensor = np.zeros((3,3))
                try:
                    if int(line[0].replace('.','')) in mos:
                        dummy_tensor += np.array([line[1:4],line[4:7],line[7:10]],dtype=float).T
                except:
                    pass

    return atoms, np.asarray(ref_coords,dtype=float), np.asarray(ref_tensors,dtype=float)


def write_gsn_input(atoms, dummies, charge, multiplicity, outfile):
    """Write a Gaussian input file for a NICS calculation

    Args:
        atoms (dict): a dictionary of Atom objects
        dummies (list/array): NICS probe coordinates
        charge (int): charge of the system
        multiplicity (int): multiplicity of the system
        outfile (str): name of the file to be written
    """
    with open(outfile, 'w') as f:
        f.write(fileman._gsn_header.format(charge=charge, multiplicity=multiplicity))
    fileman.xyz.write_xyz(atoms, outfile, write_type = 'a')
    fileman.xyz.write_dummy_xyz(dummies, outfile, write_type = 'a', dummy_label = 'Bq')
    with open(outfile, 'a') as f:
        f.write('\n')