# This file is part of bcwizard.
#
# Copyright (C) 2021 ETH Zurich, Eno Paenurk, Renana Gershoni-Poranne
#
# bcwizard is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bcwizard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with bcwizard. If not, see <https://www.gnu.org/licenses/>.

"""
Read & write ORCA 5 files
Part of bcwizard, available at:
gitlab.com/porannegroup/bcwizard
"""

# non-standard imports
import numpy as np

# bcwizard imports
from bcwizard import const, mol
from bcwizard import fileman


def read_orca_log(orca_logfile, ncs=False, mos=None):
    """Read ORCA log file

    Args:
        orca_logfile (str): Path to the ORCA log file
        ncs (bool, optional): Read NCS results from the log. Defaults to False.
        mos (str, optional): Comma-separated list of MO numbers (if ncs=True). Defaults to None.

    Returns:
        atoms, ref_coords, ref_tensors
    """

    # initalize the result containers
    atoms = {}
    ref_coords = []
    ref_tensors = []
    ref_idxs = []

    # read the log file
    with open(orca_logfile, 'r') as f:
        content = f.readlines()

    # initialize conditions used for reading the log
    read_xyz = False
    counter = False
    count = 3

    read_shifts = False
    read_nics = False
    read_sigma = False
    sigma_count = 3

    if ncs:
        dummy_tensor = np.zeros((3,3))
    else:
        dummy_tensor = []

    check_mo = False

    # loop over all the lines
    for line in content:

        # for getting xyz
        if counter:
            count -= 1
            if count == 0:
                read_xyz = True
                counter = False

        if 'CARTESIAN COORDINATES (A.U.)' in line:
            counter = True     
        if line == '\n':
            read_xyz = False

        if read_xyz:
            line = line.split()
            element = line[1]
            atomID = int(line[0]) 
            x,y,z = float(line[5]), float(line[6]), float(line[7])
            x,y,z = x/const.ang2bohr, y/const.ang2bohr, z/const.ang2bohr
            Z_test = float(line[2])
            if Z_test > 0:
                atoms[atomID] = mol.Atom(atomID + 1, element, x, y, z)
            else:
                ref_coords.append([x, y, z])
                ref_idxs.append(atomID)


        # for getting sigma values
        if not ncs:
            if 'CHEMICAL SHIFTS' in line:
                read_shifts = True
                continue

            if read_shifts and 'Nucleus' in line:
                atom = line.split()[1]
                if atom[-1] == 'H' and int(atom.replace('H','')) in ref_idxs:
                    read_nics = True
                    continue

            if read_nics and 'Total shielding tensor' in line:
                read_sigma = True
                read_nics = False
                continue

            if read_sigma:
                sigma_count -= 1
                line = line.split()
                dummy_tensor.append(line)

                if sigma_count == 0:
                    ref_tensors.append(dummy_tensor)
                    sigma_count = 3
                    read_sigma = False
                    dummy_tensor = []

        else:
            if check_mo and 'Canonical MO contributions' in line:
                read_sigma = True
                check_mo = False
                continue

            if 'Full Cartesian NMR shielding tensor (ppm) for atom gh' in line:
                check_mo = True

            if read_sigma:
                line = line.split()
                if 'Total' in line:
                    ref_tensors.append(dummy_tensor)
                    read_sigma = False
                    dummy_tensor = np.zeros((3,3))
                try:
                    if int(line[0].replace('.','')) in mos:
                        dummy_tensor += np.array([line[1:4],line[4:7],line[7:10]],dtype=float).T
                except:
                    pass

    return atoms, np.asarray(ref_coords,dtype=float), np.asarray(ref_tensors,dtype=float)


def write_orca_input(atoms, dummies, charge, multiplicity, outfile):
    """Write an ORCA input file for a NICS calculation

    Args:
        atoms (dict): a dictionary of Atom objects
        dummies (list/array): NICS probe coordinates
        charge (int): charge of the system
        multiplicity (int): multiplicity of the system
        outfile (str): name of the file to be written
    """
    with open(outfile, 'w') as f:
        f.write(fileman._orca_header.format(charge=charge, multiplicity=multiplicity))
    fileman.xyz.write_xyz(atoms, outfile, write_type = 'a')
    # Must use ghost atoms with tight s basis functions instead of dummy to create grid
    # ORCA 5 manual section 9.42.3.7
    basis_string = 'NewGTO S 1 1 1e6 1 end NewAuxJGTO S 1 1 2e6 1 end'
    with open(outfile, 'a') as f:
        for dummy in dummies:
            f.write("%-5s %14.8f %14.8f %14.8f %54s\n" % ('H:', *dummy, basis_string))
        f.write('*')
        