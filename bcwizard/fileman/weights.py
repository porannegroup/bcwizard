# This file is part of bcwizard.
#
# Copyright (C) 2021 ETH Zurich, Eno Paenurk, Renana Gershoni-Poranne
#
# bcwizard is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bcwizard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with bcwizard. If not, see <https://www.gnu.org/licenses/>.

"""
Read & write weight files
Part of bcwizard, available at:
gitlab.com/porannegroup/bcwizard
"""

def read_weights(weightsfile):
    """Read weights from file

    Args:
        weightsfile (str): name/path of the weights file

    Returns:
        dict: dictionary of rings (labeled by atom lists) with weights
    """
    weights = {}
    with open(weightsfile, 'r') as f:
        lines = f.readlines()
    for line in lines:
        line = line.split()
        atoms = line[0]
        weights[atoms] = float(line[1])
    return weights


def write_weights(molecule, outfile: str, write_type = 'a'):
    """Write xyz file

    Args:
        molecule: mol.Molecule instance
        outfile (str): name/path of the output file
        write_type (str, optional): Overwrite (w) or append (a) to the file. Defaults to 'a'.
    """
    with open(outfile, write_type) as f:
        for cycle in molecule.cycles:
            atom_list = [str(a) for a in molecule.cycles[cycle].atomIDs]
            cycle_str = ','.join(atom_list)
            f.write("%-30s %6.3f\n" % (cycle_str, molecule.cycles[cycle].weight))