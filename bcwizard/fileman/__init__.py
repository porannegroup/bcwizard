# This file is part of bcwizard.
#
# Copyright (C) 2021 ETH Zurich, Eno Paenurk, Renana Gershoni-Poranne
#
# bcwizard is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bcwizard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with bcwizard. If not, see <https://www.gnu.org/licenses/>.

# standard imports
import importlib.resources as pkg_resources

# bcwizard imports
import bcwizard.fileman.bcs
import bcwizard.fileman.gsn
import bcwizard.fileman.nics
import bcwizard.fileman.weights
import bcwizard.fileman.xyz
import bcwizard.fileman.orca


# define headers for input generation
_gsn_header = pkg_resources.read_text(__package__, 'gsn_inp_header.txt')
_orca_header = pkg_resources.read_text(__package__, 'orca_inp_header.txt')