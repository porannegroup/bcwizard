# This file is part of bcwizard.
#
# Copyright (C) 2021 ETH Zurich, Eno Paenurk, Renana Gershoni-Poranne
#
# bcwizard is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bcwizard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with bcwizard. If not, see <https://www.gnu.org/licenses/>.

"""
Read & write dummy atom (NICS probes) files
Part of bcwizard, available at:
gitlab.com/porannegroup/bcwizard
"""

# non-standard imports
import numpy as np

# bcwizard imports
from bcwizard import dummyman

def read_nics_xyz(nics_xyzfile):
    """Read NICS values and coordinates from  file

    Args:
        nics_xyzfile (str): name/path of the file

    Returns:
        np.array: array of nics values (column 0) with xyz coordinates (columns 1-3)
    """
    nics_array = np.loadtxt(nics_xyzfile)
    return nics_array

def write_nics_xyz(nics, nics_coords, outfile, write_type = 'a'):
    """Write NICS coordinate file

    Args:
        nics (list/np.array): NICS values
        nics_coords (list/np.array): NICS coordinates
        outfile (str): name/path of the output file
        write_type (str, optional): Overwrite (w) or append (a) to the file. Defaults to 'a'.
    """
    with open(outfile, write_type) as f:
        for idx in range(len(nics)):
            f.write("%-8.3f %14.8f %14.8f %14.8f\n" % (nics[idx], *nics_coords[idx]))

def write_nics_per_ring(molecule, nics, nics_coords, outfile: str, write_type = 'a'):
    """Write a file with NICS value for each ring

    Args:
        molecule: mol.Molecule instance
        nics (list/np.array): NICS values
        nics_coords (list/np.array): NICS coordinates
        outfile (str): name/path of the output file
        write_type (str, optional): Overwrite (w) or append (a) to the file. Defaults to 'a'.
    """
    if not len(molecule.cycles) == len(nics):
        print('The number of rings must equal the number of NICS values.\n')
        return
    elif len(molecule.cycles) > 1:
        reordering = dummyman.assign_centers(molecule, nics_coords)
        nics = nics[reordering]

    with open(outfile, write_type) as f:
        for cycle in molecule.cycles:
            atom_list = [str(a) for a in molecule.cycles[cycle].atomIDs]
            cycle_str = ','.join(atom_list)
            f.write("%-30s %6.3f\n" % (cycle_str, nics[cycle-1]))