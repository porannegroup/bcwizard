# This file is part of bcwizard.
#
# Copyright (C) 2021 ETH Zurich, Eno Paenurk, Renana Gershoni-Poranne
#
# bcwizard is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bcwizard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with bcwizard. If not, see <https://www.gnu.org/licenses/>.

"""
Read & write bond current files
Part of bcwizard, available at:
gitlab.com/porannegroup/bcwizard
"""

def read_bc_file(bcfile):
    """Read bond currents from file

    Args:
        bcfile (str): name/path of the bond currents file

    Returns:
        dict: dictionary of bonds (labeled by atoms) with bond current strengths
    """
    bcs = {}
    with open(bcfile,'r') as f:
        lines = f.readlines()
        for line in lines:
            line = line.split()
            bcs[(int(line[0]),int(line[1]))] = float(line[2])
    return bcs


def write_bc_file(bcGraph, outfile: str, bnz_current: float, write_type = 'a'):
    """Write bond currents file

    Args:
        bcGraph: bond current graph object
        outfile (str): name/path of the output file
        write_type (str, optional): Overwrite (w) or append (a) to the file. Defaults to 'a'.
    """
    with open(outfile, write_type) as f:
        for bond in bcGraph.edges:
            f.write("%4i %4i %9.4f\n" % (*bond, bcGraph.edges[bond]['weight'] * bnz_current))