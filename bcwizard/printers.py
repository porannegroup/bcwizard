# This file is part of bcwizard.
#
# Copyright (C) 2021 ETH Zurich, Eno Paenurk, Renana Gershoni-Poranne
#
# bcwizard is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bcwizard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with bcwizard. If not, see <https://www.gnu.org/licenses/>.

import bcwizard

def bcw_print(text):
    print(bcwizard.__spacer__)
    print('=> ' + text)
    print(bcwizard.__spacer__)
    print('')

def bcw_warning(text):
    print(bcwizard.__spacer__)
    print('!!! ' + text)
    print(bcwizard.__spacer__)
    print('')
