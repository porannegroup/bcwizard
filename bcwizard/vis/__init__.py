# This file is part of bcwizard.
#
# Copyright (C) 2021 ETH Zurich, Eno Paenurk, Renana Gershoni-Poranne
#
# bcwizard is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bcwizard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with bcwizard. If not, see <https://www.gnu.org/licenses/>.

# standard imports
import importlib.resources as pkg_resources

# non-standard imports
import matplotlib as mpl
import numpy as np

# bcwizard imports
import bcwizard.vis.bcwplot
import bcwizard.vis.vmd

# define header for vmd visualization file
_vmd_header = pkg_resources.read_text(__package__, 'vmd_header.txt')

# Define color scheme
n_colors = 40 # number of colors in the scale

# Hex codes of the colors used to generate the gradient
colors = ['#e6f2ff','#00bcff','#0042cb','#000f14']
cmap = mpl.colors.LinearSegmentedColormap.from_list("bc_cmap", colors)
