# This file is part of bcwizard.
#
# Copyright (C) 2021 ETH Zurich, Eno Paenurk, Renana Gershoni-Poranne
#
# bcwizard is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bcwizard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with bcwizard. If not, see <https://www.gnu.org/licenses/>.

"""
Primitive plotting tool for bond currents
Part of bcwizard, available at:
gitlab.com/porannegroup/bcwizard
"""

# non-standard imports
import matplotlib as mpl
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from mpl_toolkits.mplot3d import proj3d
import numpy as np

# bcwizard imports
from bcwizard import vis


def plot_3d_molecule(molecule, bcGraph, color_list, value_list, size_scaling = 1.0, colorbar = False, center_values=[], B_vec = None):
    """bcwizard current plotting function

    Args:
        molecule: molecule object as defined in mol.py
        bcGraph: bond-current graph
        color_list (list): list of colors
        value_list (list): list of values corresponding to the colors
        size_scaling (float, optional): Scales the size of the arrows. Defaults to 1.0.
        colorbar (bool, optional): Display the color bar scale. Defaults to False.
        center_values (list, optional): Print values at the centers of the rings. Defaults to [].
        B_vec (optional): External magnetic field vector (if specified, will be displayed). Defaults to None.

    Returns:
        figure
    """

    # Set up plot
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    # Plot molecule frame
    for bond in molecule.get_molGraph(skip_hydrogen=True).edges:
        coord_i = molecule.atoms[bond[0]].get_coord()
        coord_j = molecule.atoms[bond[1]].get_coord()
        plt.plot(*np.array([coord_i, coord_j]).T, lw=0.5, alpha=0.5, color='black')

    """
    # Plot filled cycles
    for cycle in molecule.cycles.values():
        x,y,z = [],[],[]
        weight = cycle.weight
        color_idx = np.abs(value_list-abs(weight)).argmin()
        ring_color = color_list[color_idx]

        for atom in cycle.atomIDs:
            atom_coord = molecule.atoms[atom].get_coord()
            x.append(atom_coord[0])
            y.append(atom_coord[1])
            z.append(atom_coord[2])
        verts = [list(zip(x,y,z))]
        ax.add_collection3d(Poly3DCollection(verts, color=ring_color, alpha=0.3))
    """
    
    # Plot values at ring centers
    if np.any(center_values):
        for i in molecule.cycles:
            center = molecule.cycles[i].get_center()
            ax.text(center[0],center[1],center[2], "{:.1f}".format(center_values[i-1]), ha='center', va='center')

    # Plot currents
    # max allowed scaling
    max_val = np.max(value_list)

    for bond in bcGraph.edges:
        coord_i = molecule.atoms[bond[0]].get_coord()
        coord_j = molecule.atoms[bond[1]].get_coord()
        weight = bcGraph.edges[bond]['weight']
        if abs(weight) > max_val:
            weight = np.sign(weight)*max_val
        color_idx = np.abs(value_list-abs(weight)).argmin()
        bc_color = color_list[color_idx]

        if not np.isnan(weight):
            # If weight is negative, invert coordinates
            if weight < 0:
                arrow3d(ax, coord_j, coord_i, abs(weight*size_scaling), color = bc_color)
            else:
                arrow3d(ax, coord_i, coord_j, abs(weight*size_scaling), color = bc_color)
        else:
            center_coord = coord_i + (coord_j - coord_i)/2
            ax.scatter(*center_coord, marker='x', s=200, alpha=0.9, color='red')

    # Plot magnetic field
    if np.any(B_vec):
        arrow3d(ax, np.zeros(3), B_vec, 0.5, color='red')

    # plot the colorbar if requested
    if colorbar:
        norm = mpl.colors.Normalize(vmin=np.min(value_list), vmax=np.max(value_list))
        cm = mpl.cm.ScalarMappable(cmap=vis.cmap, norm=norm)
        cb = fig.colorbar(cm, shrink=0.6, aspect=20)
        cb.ax.set_title(r'$I_{\rm ref}$', fontsize=14)

    # Finalize plot
    try:
        ax.set_box_aspect([1,1,1])
    except:
        print('\nFailed to set the box aspect ratio.\nMolecule might look wonky.\nProbably need to upgrade Matplotlib to 3.4 to fix.\n')
    set_axes_equal(ax)
    ax.set_axis_off()
    ax.view_init(azim=90, elev=90)
    plt.tight_layout()

    return fig


###
# Auxiliary code from external sources
###

def arrow3d(ax, xyz1, xyz2, weight, npoints=16, head=0.45, headwidth=2.0, **kw):
    # based on: https://stackoverflow.com/questions/48587088/tube-arrows-in-python
    
    w = weight * 0.12 # width
    h = head
    hw = headwidth

    offset = xyz1
    vec = xyz2 - xyz1
    length = np.linalg.norm(vec)

    a = [[0,0],[w,0],[w,(1-h)*length],[hw*w,(1-h)*length],[0,length]]
    a = np.array(a)

    r, theta = np.meshgrid(a[:,0], np.linspace(0,2*np.pi,npoints))
    z = np.tile(a[:,1],r.shape[0]).reshape(r.shape)
    x = r*np.sin(theta)
    y = r*np.cos(theta)

    if not np.array_equal(vec/length,np.array([0,0,1])):
        rot_mat = rotation_matrix_from_vectors(np.array([0,0,1]), vec)
    else:
        rot_mat = np.eye(3)
    b2 = rot_mat.dot(np.c_[x.flatten(),y.flatten(),z.flatten()].T)
    b2 = b2.T + np.array(offset)
    x = b2[:,0].reshape(r.shape)
    y = b2[:,1].reshape(r.shape) 
    z = b2[:,2].reshape(r.shape)
    ax.plot_surface(x,y,z, **kw)


def rotation_matrix_from_vectors(vec1, vec2):
	""" Find the rotation matrix that aligns vec1 to vec2
	:param vec1: A 3d "source" vector
	:param vec2: A 3d "destination" vector
	:return mat: A transform matrix (3x3) which when applied to vec1, aligns it with vec2.
	
	https://stackoverflow.com/questions/45142959/calculate-rotation-matrix-to-align-two-vectors-in-3d-space
	"""
	a, b = (vec1 / np.linalg.norm(vec1)).reshape(3), (vec2 / np.linalg.norm(vec2)).reshape(3)
	v = np.cross(a, b)
	c = np.dot(a, b)
	s = np.linalg.norm(v)
	kmat = np.array([[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0], 0]])
	rotation_matrix = np.eye(3) + kmat + kmat.dot(kmat) * ((1 - c) / (s ** 2))
	return rotation_matrix

# Functions from @Mateen Ulhaq and @karlo
def set_axes_equal(ax: plt.Axes):
    """Set 3D plot axes to equal scale.
    
    taken from:
    https://stackoverflow.com/questions/13685386/matplotlib-equal-unit-length-with-equal-aspect-ratio-z-axis-is-not-equal-to

    Make axes of 3D plot have equal scale so that spheres appear as
    spheres and cubes as cubes.  Required since `ax.axis('equal')`
    and `ax.set_aspect('equal')` don't work on 3D.
    """
    limits = np.array([
        ax.get_xlim3d(),
        ax.get_ylim3d(),
        ax.get_zlim3d(),
    ])
    origin = np.mean(limits, axis=1)
    radius = 0.3 * np.max(np.abs(limits[:, 1] - limits[:, 0]))
    _set_axes_radius(ax, origin, radius)

def _set_axes_radius(ax, origin, radius):
    x, y, z = origin
    ax.set_xlim3d([x - radius, x + radius])
    ax.set_ylim3d([y - radius, y + radius])
    ax.set_zlim3d([z - radius, z + radius])
