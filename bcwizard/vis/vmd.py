# This file is part of bcwizard.
#
# Copyright (C) 2021 ETH Zurich, Eno Paenurk, Renana Gershoni-Poranne
#
# bcwizard is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bcwizard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with bcwizard. If not, see <https://www.gnu.org/licenses/>.

"""
VMD visualization script manager
Part of bcwizard, available at:
gitlab.com/porannegroup/bcwizard
"""

# non-standard imports
import numpy as np

# bcwizard imports
from bcwizard import vis


def write_vmd(molecule, outfile, color_list, value_list, colorbar = False, center_values=[]):
    """writes a vmd visualization script

    Args:
        molecule: molecule object as defined in mol.py
        bcGraph: bond-current graph
        color_list (list): list of colors
        value_list (list): list of values corresponding to the colors
        colorbar (bool, optional): Display the color bar scale. Defaults to False.
        center_values (list, optional): Print values at the centers of the rings. Defaults to [].

    """

    with open(outfile, 'w') as f:
        # Write the header
        f.write(vis._vmd_header)

        # Define color scheme
        f.write('\n# Define colors for bond currents\n')
        for i in range(3, vis.n_colors+3):
            rgb_str = np.array2string(color_list[i-3][:3], precision=4, separator=' ')[1:-1]
            f.write('color change rgb '+str(i)+' '+rgb_str+'\n')

        # Write the molecule frame
        f.write('\n# Draw the molecule frame\n')
        f.write('draw color 1\n')
        for bond in molecule.get_molGraph(skip_hydrogen=True).edges:
            coord_i = molecule.atoms[bond[0]].get_coord()
            coord_j = molecule.atoms[bond[1]].get_coord()
            f.write('draw line {%-7.4f %-7.4f %7.4f} {%-7.4f %-7.4f %7.4f}\n' % (*coord_i, *coord_j))

        # Write the bond currents
        f.write('\n# Draw the bond currents\n')
        for bond in molecule.bcGraph.edges:
            coord_i = molecule.atoms[bond[0]].get_coord()
            coord_j = molecule.atoms[bond[1]].get_coord()
            weight = molecule.bcGraph.edges[bond]['weight']
            if weight > 0:
                coords = (*coord_i, *coord_j)
            else:
                coords = (*coord_j, *coord_i)
            color_idx = np.abs(value_list-abs(weight)).argmin()
            f.write('draw arrow {%-7.4f %-7.4f %7.4f} {%-7.4f %-7.4f %7.4f} {%4.3f} {%3i}\n' % (*coords, abs(weight), color_idx+3))

        # Write values at the centers of the rings
        if np.any(center_values):
            f.write('\n# Write values at ring centers\n')
            f.write('draw color 2\n')
            for i in molecule.cycles:
                center = molecule.cycles[i].get_center()
                value = center_values[i-1]
                # manual centering for vmd, based on text length
                center -= np.array([0.15,0.0,0.0])*len("{:.1f}".format(value))
                f.write('draw text {%-7.4f %-7.4f %7.4f} %5.1f size 1.0\n' % (*center, value))

        
        # Calculate color bar parameters and add it if requested
        if colorbar:
            # get molecular center
            coords = []
            for atom in molecule.atoms.values():
                coords.append(atom.get_coord())
            coords = np.array(coords)
            mol_center = np.sum(coords,axis=1) / len(molecule.atoms)
            # define parameters for the color map
            x_coord = np.min(coords.T[0]) - 1
            y_coord = mol_center[1]
            height = 0.75*(np.max(coords.T[1])- np.min(coords.T[1]))
            width = 1.0
            ncolors = len(value_list)
            min_val = np.min(value_list)
            max_val = np.max(value_list)
            n_labels = 5
            f.write('\ncolor_scale_bar %4.1f %4.1f %4.1f %4.1f %5i %4.1f %4.1f %4i' % (x_coord, y_coord, height, width, ncolors, min_val, max_val, n_labels-1))

        f.write('\ndisplay resetview\n')
        f.write('\ndisplay resize 600 600\n')
        #f.write('render POV3 '+outfile.replace('.tcl','.pov'))
        #f.write('\nexit\n')