# This file is part of bcwizard.
#
# Copyright (C) 2021 ETH Zurich, Eno Paenurk, Renana Gershoni-Poranne
#
# bcwizard is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bcwizard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with bcwizard. If not, see <https://www.gnu.org/licenses/>.

"""
Interactive menu
Part of bcwizard, available at:
gitlab.com/porannegroup/bcwizard
"""

# standard imports
import pathlib as pl
import traceback

# non-standard imports
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

# bcwizard imports
import bcwizard
from bcwizard import calc
from bcwizard import fileman
from bcwizard import vis
from bcwizard import mol, dummyman
from bcwizard import printers

class InteractiveMenu():

    # coordinate indices for vectors
    coord_idx = {'x':0, 'y':1, 'z':2}

    def __init__(self):
        self.molecule = None
        self.bcGraph = None
        self.probe_coords = np.full(1, False)
        self.ref_nics = np.full(1, False)
        self.nics = None
        self.weights = None
        self.name = ''
        self.basename = 'bcw'

        self.bond_points = 100
        self.height_calc = 0.9
        self.bnz_current = 11.5
        self.alpha = -27.89
        self.beta = 2.17

        # for visual
        self.min_val = 0.0
        self.max_val = 2.5
        self.colorbar = False
        self.center_values = []
        self._update_colormap()
        self.arrow_size_scale = 1.0


        print(bcwizard.__BANNER__.format(version=bcwizard.__version__))
        self.main_menu()


    """
    Menu functions
    """

    def main_menu(self):
        # dict for functions:
        func_dict = {1: self.system_menu,
                     2: self.calc_menu,
                     3: self.nics2bc_menu,
                     4: self.bc2nics,
                     5: self.file_menu,
                     6: self.vis_menu,
                     7: self.print_all}
        # option list
        opt_list = ['Set up the system for calculations',
                    'Modify the calculation settings',
                    'Calculate bond currents from NICS (nics2bc)',
                    'Calculate NICS from bond currents (bc2nics)',
                    'Generate files',
                    'Visualize bond currents',
                    'Print information on the current state']
                    
        # run the interactive menu
        opt = self._interactive_menu(opt_list, exit_opt = True, return_opt = False)

        # Exit or call the requested menu function
        if opt == 0:
            self._exit_bcw()
        else:
            func_dict[opt]()


    def system_menu(self):

        # select options
        option_dict = {'read_xyz_file': 'Read in xyz file of the molecule',
                       'read_nics_xyz_file': 'Read in coordinate file of NICS values',
                       'read_weights_file': 'Read in file with ring weights',
                       'read_bc_file': 'Read in file with bond currents',
                       'read_gsn_log': 'Read in Gaussian log file',
                       'read_orca_log': 'Read in ORCA log file',
                       'gen_probes': 'Generate new NICS probes above the ring centers',
                       'convert_height': 'Change the NICS probes height (recommended for nics2bc or bc2nics)',
                      }

        # Instructions for executing the requested option
        def _exec_options(option):

            if option == 'read_xyz_file':
                xyz_file = self._ask_file()
                try:
                    atoms, probe_coords = fileman.xyz.read_xyz(xyz_file)
                    if not len(probe_coords) == 0:
                        self.probe_coords = np.array(probe_coords)
                    self.molecule = mol.Molecule(atoms = atoms)
                    self.name = str(xyz_file)
                    printers.bcw_print('Molecule generated from '+str(xyz_file))
                except:
                    print(traceback.format_exc())
                    printers.bcw_warning('Failed to load file.')


            elif option == 'read_nics_xyz_file':
                nics_xyz_file = self._ask_file()
                try:
                    nics_probes = fileman.nics.read_nics_xyz(nics_xyz_file)
                    self.probe_coords = nics_probes.T[1:].T
                    self.ref_nics = nics_probes.T[0].T
                    printers.bcw_print('NICS probes loaded from '+str(nics_xyz_file))
                except:
                    print(traceback.format_exc())
                    printers.bcw_warning('Failed to load file.')


            elif option == 'read_weights_file':
                if self._check_molecule():
                    weights_file = self._ask_file()
                    try:
                        weights_dict = fileman.weights.read_weights(weights_file)
                        printers.bcw_print('Weights loaded from '+str(weights_file))
                    except:
                        print(traceback.format_exc())
                        printers.bcw_warning('Failed to load file.')
                    try:
                        self.weights = []
                        for cycle in self.molecule.cycles:
                            for atoms in weights_dict:
                                atom_list = atoms.split(',')
                                atom_list = [int(a) for a in atom_list]
                                atom_list.sort()
                                if atom_list == self.molecule.cycles[cycle].atomIDs:
                                    self.weights.append(weights_dict[atoms])
                        
                        self.molecule.set_cycle_weights(self.weights)
                        self.molecule.set_bcGraph()
                        printers.bcw_print('Weights and bond currents updated.')
                    except:
                        print(traceback.format_exc())
                        printers.bcw_warning('Failed to update weights or bond currents.') 


            elif option == 'gen_probes':
                if self._check_molecule():
                    height = float(input('Enter the height (in Angstrom) for the NICS probes:\n'))
                    self.probe_coords = dummyman.gen_dummies(self.molecule, height, gentype='positive')
                    printers.bcw_print('NICS probes generated at '+str(height)+' Angstroms')


            elif option == 'read_bc_file':
                if self._check_molecule():
                    bc_file = self._ask_file()
                    try:
                        printers.bcw_print('Bond currents loaded from '+str(bc_file))
                    except:
                        print(traceback.format_exc())
                        printers.bcw_warning('Failed to load file.')
                    try:
                        bc_vals = fileman.bcs.read_bc_file(bc_file)
                        self.molecule.set_bcGraph()
                        for bond in bc_vals:
                            try:
                                self.molecule.bcGraph.edges[bond]['weight'] = bc_vals[bond] / self.bnz_current
                            except:
                                bond_rev = [bond[1],bond[0]]
                                self.molecule.bcGraph.edges[bond_rev]['weight'] = bc_vals[bond] / -self.bnz_current
                        printers.bcw_print('Bond currents updated.')
                    except:
                        print(traceback.format_exc())
                        printers.bcw_warning('Failed to update bond currents.') 


            elif option == 'read_gsn_log':
                gsn_log = self._ask_file()
                try:
                    self.name = str(gsn_log)
                    # Ask for further input
                    """
                    # asking for height to deal with the old log files for the paper (to be deleted at a later stage)
                    height = input('Enter the height of the NICS probe to load (if only one height exists in the log, press enter):\n(works only for molecules in the XY-plane)\n')
                    if not height == '':
                        height = float(height)
                    """

                    B_ext = input('Enter the external magnetic field vector for extracting NICS from the tensor (press enter to assign 0,0,1):\n')
                    if B_ext:
                        B_ext = np.array(B_ext.split(','), dtype=float)
                    else:
                        B_ext = np.array([0,0,1])

                    nics_component = self._ask_nics(action='read')

                    ncs_bool = self._ask_bool('Should NICS be read from NCS (only if available)? (y/n, press enter for n):\n', default_answer='n')
                    if ncs_bool:
                        ncs_bool = True
                        def _ask_orbs():
                            ncs_orbs = input('Enter the numbers of orbitals to be used for extracting NICS (as comma-separated list):\n')
                            if ncs_orbs == None:
                                printers.bcw_warning('No orbitals entered.')
                                _ask_orbs()
                            return ncs_orbs
                        ncs_orbs = _ask_orbs()
                    else:
                        ncs_bool = False
                    
                    # Read the log
                    # Check if NCS option exists
                    if ncs_bool:
                        mo_numbers = np.array(ncs_orbs.split(','), dtype=int)
                    else:
                        mo_numbers = None
                
                    # Read the log file
                    atoms, self.probe_coords, ref_tensors = fileman.gsn.read_gsn_log(gsn_log, ncs=ncs_bool, mos=mo_numbers)
                    # Calculate the NICS based on the tensor and B_ext
                    self.ref_nics = -np.asarray([s.dot(B_ext)[self.coord_idx[nics_component]] for s in ref_tensors])
                    # Define molecule
                    self.molecule = mol.Molecule(atoms = atoms)
                    """
                    # Remove NICS data if a specific height was specified
                    if not height == '':
                        del_list = []
                        for i in range(len(self.probe_coords)):
                            if not round(self.probe_coords[i][2],2) == height:
                                del_list.append(i)
                        self.ref_nics = np.delete(self.ref_nics, del_list)
                        self.probe_coords = np.delete(self.probe_coords, del_list, axis=0)
                    """
                    printers.bcw_print('Molecule generated and NICS probes loaded from: \n'+str(gsn_log))
                except:
                    print(traceback.format_exc())
                    printers.bcw_warning('Failed to load file.')

                if len(self.ref_nics) == 0:
                    printers.bcw_warning('Reference NICS array empty.\nThere was probably a problem in reading the file.')
                
                if len(self.probe_coords) == 0:
                    printers.bcw_warning('NICS probe coordinate array empty.\nThere was probably a problem in reading the file.')


            elif option == 'read_orca_log':
                orca_log = self._ask_file()
                try:
                    self.name = str(orca_log)
                    # Ask for further input
                    """
                    height = input('Enter the height of the NICS probe to load (if only one height exists in the log, press enter):\n(works only for molecules in the XY-plane)\n')
                    if height:
                        height = float(height)
                    """

                    B_ext = input('Enter the external magnetic field vector for extracting NICS from the tensor (press enter to assign 0,0,1):\n')
                    if B_ext:
                        B_ext = np.array(B_ext.split(','), dtype=float)
                    else:
                        B_ext = np.array([0,0,1])

                    nics_component = self._ask_nics(action='read')

                    ncs_bool = self._ask_bool('Should NICS be read from NCS (only if available)? (y/n, press enter for n):\n', default_answer='n')
                    if ncs_bool:
                        ncs_bool = True
                        def _ask_orbs():
                            ncs_orbs = input('Enter the numbers of orbitals to be used for extracting NICS (as comma-separated list):\n')
                            if ncs_orbs == None:
                                printers.bcw_warning('No orbitals entered.')
                                _ask_orbs()
                            return ncs_orbs
                        ncs_orbs = _ask_orbs()
                    else:
                        ncs_bool = False
                    
                    # Read the log
                    # Check if NCS option exists
                    if ncs_bool:
                        mo_numbers = np.array(ncs_orbs.split(','), dtype=int)
                    else:
                        mo_numbers = None
                
                    # Read the log file
                    atoms, self.probe_coords, ref_tensors = fileman.orca.read_orca_log(orca_log, ncs=ncs_bool, mos=mo_numbers)
                    # Calculate the NICS based on the tensor and B_ext
                    self.ref_nics = -np.asarray([s.dot(B_ext)[self.coord_idx[nics_component]] for s in ref_tensors])
                    # Define molecule
                    self.molecule = mol.Molecule(atoms = atoms)
                    """
                    # Remove NICS data if a specific height was specified
                    if height:
                        del_list = []
                        for i in range(len(self.probe_coords)):
                            if not round(self.probe_coords[i][2],2) == height:
                                del_list.append(i)
                        self.ref_nics = np.delete(self.ref_nics, del_list)
                        self.probe_coords = np.delete(self.probe_coords, del_list, axis=0)
                    """
                    printers.bcw_print('Molecule generated and NICS probes loaded from: \n'+str(orca_log))
                except:
                    print(traceback.format_exc())
                    printers.bcw_warning('Failed to load file.')

                if len(self.ref_nics) == 0:
                    printers.bcw_warning('Reference NICS array empty.\nThere was probably a problem in reading the file.')
                
                if len(self.probe_coords) == 0:
                    printers.bcw_warning('NICS probe coordinate array empty.\nThere was probably a problem in reading the file.')

    
            elif option == 'convert_height':
                height_opt = input('Enter new height (in Angstrom) or type "opt" to optimize:\n')
                if height_opt == 'opt':

                    ref_nics = float(input('Enter reference NICS value:\n'))

                    # Set up reference benzene
                    benzene_atoms = {}
                    for i, xy in enumerate(calc._bnz_C_xy_coords):
                        benzene_atoms[i+1] = mol.Atom(i+1, 'C', xy[0], xy[1], 0.0)
                    benzene = mol.Molecule(benzene_atoms)
                    benzene.set_cycle_weights([1])
                    benzene.set_bcGraph()

                    # Set up Biot-Savart calculator
                    bs_calculator = calc.biotsavart.BiotSavart(benzene, 
                                                self.bnz_current * 1e-9,
                                                n_points = self.bond_points, 
                                                unit = 'au') # could be a variable later

                    nics_component = self._ask_nics('optimize')
                    new_height = calc.height.opt_height(benzene, bs_calculator, ref_nics, component = nics_component)
                    printers.bcw_print('Optimized height: '+str(round(new_height,4)))
                else:
                    new_height = float(height_opt)

                height_type = input('Change height for NICS-per-ring (default, press enter) or all Z coordinates (enter z)\n')
                if not height_type:
                    if len(self.molecule.cycles) == len(self.probe_coords):
                        self.probe_coords = dummyman.adjust_ref_height(self.molecule, self.probe_coords, new_height)
                        printers.bcw_print('Height updated to '+ str(round(new_height,4)))
                    else:
                        printers.bcw_warning('Number of rings should equal number of NICS probes.\nHeight not updated.')

                elif height_type == 'z':
                    self.probe_coords[:,2] = new_height
                    printers.bcw_print('Z coordinate of NICS probes changed to '+str(round(new_height,4)))
                
                else:
                    printers.bcw_warning(height_type+' not recognized. Height not updated')


        # Run the default menu style with the respective data
        self._default_menu(option_dict, self.system_menu, _exec_options,
                           exit_opt=True, return_opt=True)


    def calc_menu(self):

        # select options
        option_dict = {'bond_points':'Set number of points per bond for the integration (current value = '+str(self.bond_points)+')',
                       'bnz_current': 'Set the reference benzene current (in nA/T) (current value = '+str(self.bnz_current)+')',
                       'alpha': 'Set the alpha value for Huckel-like calculations (current value = '+str(self.alpha)+')',
                       'beta': 'Set the beta value for Huckel-like calculations (current value = '+str(self.beta)+')'
                      }

        # Instructions for executing the requested option
        def _exec_options(option):
            if option == 'set_bond_points':
                self.bond_points = int(self._ask_value())
            else:
                setattr(self, option, float(self._ask_value()))

            printers.bcw_print('Value updated.')

        # Run the default menu style with the respective data
        self._default_menu(option_dict, self.calc_menu, _exec_options,
                           exit_opt=True, return_opt=True)


    def nics2bc_menu(self):
        if self._check_molecule() & self._check_probes() & self._check_nics():
            pass
        else:
            self.main_menu()

        # select options
        option_dict = {'bc_matrix': 'Use the matrix equation with bond currents to calculate ring weights',
                       'huckel_matrix': 'Use the Huckel-like approximation to calculate ring weights',
                       'bc_simplex': 'Use the Simplex method with bond currents to numerically optimize ring weights'
                      }

        # Instructions for executing the requested option
        def _exec_options(option):

            if 'bc' in option:
                # Generate bcGraph if missing
                self.molecule.set_bcGraph()
                # Initialize Biot-Savart calculator
                bs_calculator = calc.biotsavart.BiotSavart(self.molecule, 
                                                self.bnz_current * 1e-9,
                                                n_points = self.bond_points, 
                                                unit = 'au') # could be a variable later
                # ask for nics component
                nics_component = self._ask_nics(action='calculate')

            if option == 'bc_matrix':
                if len(self.molecule.cycles) > len(self.ref_nics):
                    printers.bcw_warning('The number of rings must not be larger than the number of reference NICS values.')
                    self.nics2bc_menu()
                else:
                    self.weights = calc.nics2bc.bc_matrix_calc(self.molecule, bs_calculator,
                                                           self.ref_nics, self.probe_coords,
                                                           component=nics_component)

            elif option == 'huckel_matrix':
                if not len(self.molecule.cycles) == len(self.ref_nics):
                    printers.bcw_warning('The number of rings must equal the number of reference NICS values.')
                    self.nics2bc_menu()
                else:
                    self.weights = calc.nics2bc.huckel_matrix_calc(self.molecule, self.ref_nics,
                                                          alpha = self.alpha, beta = self.beta)

            elif option == 'bc_simplex':
                print(bcwizard.__spacer__)
                _, self.weights = calc.nics2bc.bc_simplex_opt(self.molecule, bs_calculator,
                                                            self.ref_nics, self.probe_coords,
                                                            component=nics_component)
                print(bcwizard.__spacer__)


            self.molecule.set_cycle_weights(self.weights)
            self.molecule.set_bcGraph()
            printers.bcw_print('Weights calculated, bond currents updated')

        # Run the default menu style with the respective data
        self._default_menu(option_dict, self.nics2bc_menu, _exec_options,
                           exit_opt=True, return_opt=True)


    def bc2nics(self):
        if self._check_molecule() & self._check_probes() & self._check_bcGraph():            

            bs_calculator = calc.biotsavart.BiotSavart(self.molecule, 
                                                self.bnz_current * 1e-9,
                                                n_points = self.bond_points, 
                                                unit = 'au') # could be a variable later
            
            nics_component = self._ask_nics(action='calculate')
            self.nics = bs_calculator.calc_nics(self.probe_coords, component=nics_component)
            printers.bcw_print('NICS calculated')

        self.main_menu()


    def vis_menu(self):
        # select options
        option_dict = {'basename':'Set basename for files (currently: '+self.basename+')',
                       'range': 'Set the minimum and maximum value for scaling (currently: min='+str(self.min_val)+', max='+str(self.max_val)+')',
                       'center_values': 'Show values at ring centers',
                       'colorbar': 'Display the color scale bar (currently: '+str(self.colorbar)+')',
                       'size_scale': 'Change the arrow size scale (currently: '+str(self.arrow_size_scale)+')',
                       'vmd': 'Write a visualization script for VMD',
                       'show': 'Show the bond currents with the BC-Wizard visualizer',
                       'plot': 'Save the image with the BC-Wizard visualizer'
                      }

        # Instructions for executing the requested option
        def _exec_options(option):
            if option == 'basename':
                self.basename = input('Enter new basename:\n')

            elif option == 'range':
                try:
                    self.min_val = float(input('Enter new minimum value:\n'))
                    self.max_val = float(input('Enter new maximum value:\n'))
                    self._update_colormap()
                except:
                    print(traceback.format_exc())
                    printers.bcw_warning('Updating values failed')

            elif option == 'center_values':
                try:
                    center_values = input('Which values to show at ring centers (options: none, nics, weights)?\n')
                    if center_values == 'none':
                        self.center_values = []

                    elif center_values == 'nics':
                        nics_type = input('Show BC2NICS values (bc2nics, default) or imported NICS values (imported)?\n')
                        if not nics_type or nics_type == 'bc2nics':
                            self.center_values = self.nics
                        elif nics_type == 'imported':
                            self.center_values = self.ref_nics
                        else:
                            printers.bcw_warning(nics_type+' keyword not recognized')
                            return

                        if len(self.molecule.cycles) == len(self.center_values):
                                idxs = dummyman.assign_centers(self.molecule, self.probe_coords)
                                self.center_values = self.center_values[idxs]
                        else:
                                printers.bcw_warning('Number of values should equal the number of rings.\nRing center values not set.')

                    elif center_values == 'weights':
                        self.center_values = []
                        for cycle in self.molecule.cycles.values():
                            self.center_values.append(cycle.weight)
                    else:
                        printers.bcw_warning(center_values+' keyword not recognized')
                except:
                    print(traceback.format_exc())
                    printers.bcw_warning('Updating center values failed')

            elif option == 'colorbar':
                self.colorbar = self._ask_bool('Show color scale bar? (y/n, press enter for n)\n', default_answer='n')
            
            elif option == 'size_scale':
                self.arrow_size_scale = float(self._ask_value())

                    
            elif self._check_molecule() & self._check_bcGraph():
                if option == 'vmd':
                    vis.vmd.write_vmd(self.molecule, self.basename+'_vmd.tcl',
                                      self.color_list, self.value_list, 
                                      colorbar = self.colorbar, center_values=self.center_values)
                    printers.bcw_print(self.basename+'_vmd.tcl written')
                else:
                    bc_fig = vis.bcwplot.plot_3d_molecule(self.molecule, self.molecule.bcGraph,
                                                          self.color_list, self.value_list, size_scaling=self.arrow_size_scale,
                                                          colorbar = self.colorbar, center_values=self.center_values)
                    if option == 'show':
                        plt.show()
                    elif option == 'plot':
                        extension = input('Enter the extension for the image file\n(default: png, options: pdf, eps, svg, ...)\n')
                        if not extension:
                            extension = 'png'
                        try:
                            bc_fig.savefig(self.basename+'_bcw.'+extension, format=extension, dpi=300)
                            printers.bcw_print(self.basename+'_bcw.'+extension+' written')
                        except:
                            print(traceback.format_exc())
                            printers.bcw_warning('Writing the file failed')

        # Run the default menu style with the respective data
        self._default_menu(option_dict, self.vis_menu, _exec_options,
                           exit_opt=True, return_opt=True)


    def file_menu(self):
        # select options
        option_dict = {'basename':'Set basename for files (currently: '+self.basename+')',
                       'write_xyz': 'Write an xyz file of the molecule',
                       'write_coord_nics': 'Write a coordinates file of the NICS values',
                       'write_ring_nics': 'Write a NICS-per-ring file',
                       'write_weights': 'Write a ring weights file',
                       'write_bcs': 'Write a bond currents file',
                       'write_gsn_inp': 'Write a Gaussian input file',
                       'write_orca_inp': 'Write an ORCA 5 input file'
                      }

        # Instructions for executing the requested option
        def _exec_options(option):
            if option == 'basename':
                self.basename = input('Enter new basename:\n')
                print('')

            elif option == 'write_xyz':
                if self._check_molecule():
                    dummies = self._ask_bool('Write dummy atoms into xyz? (y/n, press enter for n)\n', default_answer='n')
                    try:
                        fileman.xyz.write_xyz(self.molecule.atoms, self.basename+'.xyz',
                                        write_type='w', xyz_head=True)
                        if dummies:
                            if self._check_probes():
                                fileman.xyz.write_dummy_xyz(self.probe_coords, self.basename+'.xyz',
                                                            write_type='a', dummy_label='X')
                        printers.bcw_print(self.basename+'.xyz written')
                    except:
                        print(traceback.format_exc())
                        printers.bcw_warning('Writing failed')
                

            elif option == 'write_coord_nics':
                if self._check_probes():
                    nics_type = input('Write BC2NICS values (bc2nics, default) or imported NICS values (imported)?\n')
                    if not nics_type or nics_type == 'bc2nics':
                        write_nics = self.nics
                    elif nics_type == 'imported':
                        write_nics = self.ref_nics
                    else:
                        printers.bcw_warning(nics_type+' keyword not recognized')
                        return

                    try:
                        fileman.nics.write_nics_xyz(write_nics, self.probe_coords, self.basename+'_nics_coords.dat',
                                                    write_type='w')
                        printers.bcw_print(self.basename+'_nics_coords.dat written')
                    except:
                        print(traceback.format_exc())
                        printers.bcw_warning('Writing failed')

            elif option == 'write_ring_nics':
                if self._check_probes():
                    nics_type = input('Write BC2NICS values (bc2nics, default) or imported NICS values (imported)?\n')
                    if not nics_type or nics_type == 'bc2nics':
                        write_nics = self.nics
                    elif nics_type == 'imported':
                        write_nics = self.ref_nics
                    else:
                        printers.bcw_warning(nics_type+' keyword not recognized')
                        return

                    if len(self.molecule.cycles) == len(write_nics):
                            idxs = dummyman.assign_centers(self.molecule, self.probe_coords)
                            write_nics = write_nics[idxs]
                    else:
                            printers.bcw_warning('Number of NICS values should equal the number of rings.')
                            return
                        
                    try:
                        fileman.nics.write_nics_per_ring(self.molecule, write_nics, self.probe_coords, 
                                                    self.basename+'_nics_per_ring.dat',
                                                    write_type='w')
                        printers.bcw_print(self.basename+'_nics_per_ring.dat written')
                    except:
                        print(traceback.format_exc())
                        printers.bcw_warning('Writing failed')

            elif option == 'write_weights':
                try:
                    fileman.weights.write_weights(self.molecule, self.basename+'_weights.dat',
                                                write_type='w')
                    printers.bcw_print(self.basename+'_weights.dat written')
                except:
                    print(traceback.format_exc())
                    printers.bcw_warning('Writing failed')

            elif option == 'write_bcs':
                if self._check_bcGraph():
                    try:
                        fileman.bcs.write_bc_file(self.molecule.bcGraph, self.basename+'_bcs.dat',
                                                self.bnz_current, write_type='w')
                        printers.bcw_print(self.basename+'_bcs.dat written')
                    except:
                        print(traceback.format_exc())
                        printers.bcw_warning('Writing failed')

            elif option == 'write_gsn_inp':
                if self._check_molecule() & self._check_probes():
                    chrg_mult = input('Enter the charge and multiplicity (space-separated; press enter for 0 1):\n')
                    if not chrg_mult:
                        chrg_mult = '0 1'
                    try:
                        charge = int(chrg_mult.split(' ')[0])
                        mult = int(chrg_mult.split(' ')[1])
                        fileman.gsn.write_gsn_input(self.molecule.atoms, self.probe_coords, 
                                                    charge=charge, multiplicity=mult,
                                                    outfile=self.basename+'_nics.in')
                        printers.bcw_print(self.basename+'_nics.in written')
                    except:
                        print(traceback.format_exc())
                        printers.bcw_warning('Writing failed')

            elif option == 'write_orca_inp':
                if self._check_molecule() & self._check_probes():
                    chrg_mult = input('Enter the charge and multiplicity (space-separated; press enter for 0 1):\n')
                    if not chrg_mult:
                        chrg_mult = '0 1'
                    try:
                        charge = int(chrg_mult.split(' ')[0])
                        mult = int(chrg_mult.split(' ')[1])
                        fileman.orca.write_orca_input(self.molecule.atoms, self.probe_coords, 
                                                    charge=charge, multiplicity=mult,
                                                    outfile=self.basename+'_nics.inp')
                        printers.bcw_print(self.basename+'_nics.inp written')
                    except:
                        print(traceback.format_exc())
                        printers.bcw_warning('Writing failed')
                    


        # Run the default menu style with the respective data
        self._default_menu(option_dict, self.file_menu, _exec_options,
                           exit_opt=True, return_opt=True)

    def print_all(self):
        print(bcwizard.__spacer__)
        print('Molecule loaded from ' + self.name)
        print('\nCurrent calculation settings:')
        print('Reference benzene current: '+str(self.bnz_current)+' nA/T')
        print('Number of points per bond for integration: '+str(self.bond_points))
        print('Alpha for Huckel-like calculation: '+str(self.alpha))
        print('Beta for Huckel-like calculation: '+str(self.beta))
        print('\nSystem-related information:')
        np.set_printoptions(precision=3)
        print('Weights:\n', self.weights)
        print('Calculated NICS values:\n', self.nics)
        print('Reference NICS values:\n', self.ref_nics)
        np.set_printoptions(precision=8)
        print('NICS probe coordinates:\n', self.probe_coords)
        print(bcwizard.__spacer__)
        print('')
        self.main_menu()



    """
    Auxiliary functions
    """

    def _default_menu(self, list_of_dicts, menu, exec_options, exit_opt = True, return_opt = True):
        # generate dict and list of options:
        opt_list, opt_dict = self._gen_list_and_dict(list_of_dicts)
        
        # run the interactive menu
        opt = self._interactive_menu(opt_list, exit_opt = True, return_opt = True)

        # execute input requests
        if opt > 0:
            exec_options(opt_dict[opt])

        # exit or continue looping
        self._exec_navigation(opt, menu)


    def _interactive_menu(self, opt_list, exit_opt = True, return_opt = True):
        extra_opts = 0
        if exit_opt:
            extra_opts += 1
        if return_opt:
            extra_opts += 1

        while True:
            # ask for input
            inp = input(self._setup_opts(opt_list, exit_opt, return_opt))
            # print empty line after option
            print('')

            # convert input to int if possible
            try:
                inp = int(inp)
            except:
                pass

            # if input in the expected range, break loop
            if inp in range(1 - extra_opts,len(opt_list)+1):
                break
            else:
                printers.bcw_warning(str(inp)+' is not a valid option')

        return inp


    def _setup_opts(self, list, exit_opt=True, return_opt=True):
        # set up option string
        opt_str = 'Enter the number to select the option:\n'
        if return_opt:
            opt_str += '-1 - Return to main menu\n'
        if exit_opt:
            opt_str += '0 - Exit BC-Wizard\n'

        for i, string in enumerate(list):
            opt_str += str(i+1)+' - ' + string + '\n'

        return opt_str


    def _gen_list_and_dict(self, option_dict):
        opt_list = []
        opt_dict = {}
        idx = 1
        for opt in option_dict:
            opt_list.append(option_dict[opt])
            opt_dict[idx] = opt
            idx += 1
        return opt_list, opt_dict


    def _exec_navigation(self, inp, menu):
        if inp == -1:
            self.main_menu()
        elif inp == 0:
            self._exit_bcw()
        else:
            menu()


    def _ask_file(self):
        return pl.Path(input('Enter the filename:\n'))

    def _ask_value(self):
        return input('Enter new value:\n')

    def _ask_bool(self, input_request, default_answer = 'n'):
        inp = input(input_request)
        if inp == '':
            inp = default_answer

        if inp == 'y':
            return True
        elif inp == 'n':
            return False
        else:
            printers.bcw_warning('Only y/n accepted as input')
            return self._ask_bool(input_request, default_answer=default_answer)

    def _ask_nics(self, action):
        nics_component = input('Enter which NICS component to '+action+' (options: x,y,z; press enter to choose z):\n')
        if not nics_component:
            nics_component = 'z'
        if nics_component not in self.coord_idx.keys():
            printers.bcw_warning(nics_component+' not recognized as a valid component')
            return self._ask_nics(action)
        else:
            return nics_component

    def _check_molecule(self):
        if not self.molecule:
            printers.bcw_warning('Molecule must be defined first. \nRead in xyz or Gaussian log file.')
            return False
        else:
            return True

    def _check_probes(self):
        if not self.probe_coords.any():
            printers.bcw_warning('Coordinates for NICS probes must be defined first. \nRead in probe coordinates, Gaussian log file, or generate new.')
            return False
        else:
            return True

    def _check_nics(self):
        if not self.ref_nics.any():
            printers.bcw_warning('NICS values must be defined first. \nRead in probe coordinate file or Gaussian log file.')
            return False
        else:
            return True

    def _check_bcGraph(self):
        if not hasattr(self.molecule, 'bcGraph'):
            printers.bcw_warning('Bond currents must be defined first. \nRead in weights or bc file, or generate with nics2bc.')
            return False
        else:
            return True

    def _update_colormap(self):
        norm = mpl.colors.Normalize(vmin=self.min_val, vmax=self.max_val)
        self.value_list = np.linspace(self.min_val, self.max_val, vis.n_colors)
        self.color_list = vis.cmap(norm(self.value_list))

    def _exit_bcw(self):
        print('Exiting BC-Wizard')
        exit(0)